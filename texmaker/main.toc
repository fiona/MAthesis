\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {english}
\select@language {english}
\contentsline {chapter}{Declaration of Authorship}{i}{section*.1}
\contentsline {chapter}{Abstract}{ii}{section*.2}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.7}
\contentsline {section}{\numberline {1.1}Motivation}{1}{section.8}
\contentsline {section}{\numberline {1.2}Biological background}{2}{section.9}
\contentsline {section}{\numberline {1.3}Mathematical background}{4}{section.12}
\contentsline {section}{\numberline {1.4}Related work}{7}{section.14}
\contentsline {section}{\numberline {1.5}Thesis structuring}{8}{section.15}
\contentsline {chapter}{\numberline {2}Methods}{9}{chapter.16}
\contentsline {section}{\numberline {2.1}General problem formulation }{9}{section.17}
\contentsline {section}{\numberline {2.2}Analytical solution}{10}{section.21}
\contentsline {subsection}{\numberline {2.2.1}Explicit solution for 2-step pathways}{10}{subsection.22}
\contentsline {subsubsection}{Definition of the transition time}{11}{section*.29}
\contentsline {subsubsection}{The assumption of stepwise constant enzyme concentrations}{12}{section*.35}
\contentsline {subsubsection}{Minimizing the transition time}{21}{section*.89}
\contentsline {subsection}{\numberline {2.2.2}Pontryagin's Maximum Principle}{24}{subsection.100}
\contentsline {section}{\numberline {2.3}Numerical methods}{27}{section.109}
\contentsline {subsection}{\numberline {2.3.1}Dynamic Optimization: Overview and classification}{27}{subsection.110}
\contentsline {subsection}{\numberline {2.3.2}Sequential Approach: Control Vector Parameterization}{30}{subsection.121}
\contentsline {subsection}{\numberline {2.3.3}Multiple Shooting}{32}{subsection.123}
\contentsline {subsection}{\numberline {2.3.4}Simultaneous Approach: Direct Transcription}{36}{subsection.136}
\contentsline {subsection}{\numberline {2.3.5}NLP methods overview}{45}{subsection.158}
\contentsline {subsubsection}{Interior Point Optimization (IPOPT)}{45}{section*.160}
\contentsline {subsubsection}{Sequential Quadratic Programming (SQP)}{46}{section*.166}
\contentsline {subsubsection}{Stochastic Ranking Evolution Strategy (SRES)}{47}{section*.167}
\contentsline {chapter}{\numberline {3}Results}{48}{chapter.168}
\contentsline {section}{\numberline {3.1}Sequential Approach: Control Vector Parameterization}{48}{section.169}
\contentsline {section}{\numberline {3.2}Simultaneous Approach: Direct Transcription}{51}{section.172}
\contentsline {section}{\numberline {3.3}Multiple Shooting}{53}{section.175}
\contentsline {chapter}{\numberline {4}Conclusion}{58}{chapter.181}
\contentsline {chapter}{Bibliography}{61}{chapter*.182}
