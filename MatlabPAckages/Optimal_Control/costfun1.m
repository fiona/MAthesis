function J = costfun1( x0, ts, dvar, rho, N, beta, optODE )
%COSTFUN1 cost function for piecewise constant control
f = myfunint1( x0,N,ts,dvar,beta,optODE );
x1T = dvar(2*N+1);
x2T = dvar(2*N+2);
J = rho*((x1T-4)^2+(x2T-4)^2) + f(end);

end