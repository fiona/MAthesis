function dx = de( t,x,u,v,z,ks )
%DYNEQN1 dynmaic equation of the system with piecewise constant control
dx = zeros(4,1);
dx(1) = -x(1)*u(ks);
dx(2) = x(1)*u(ks)-x(2)*v(ks);
dx(3) = x(2)*v(ks)-x(3)*z(ks);
dx(4) = x(3)*z(ks);
end