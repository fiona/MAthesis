function J = costfun1( x0, ts, dvar, N, optODE )
%COSTFUN1 cost function for piecewise constant control
f = myfunint1( x0,N,ts,dvar,optODE );
x1T = dvar(2*N+1);
x2T = dvar(2*N+2);
J = x1+x2+x3 + f(end);

end