#define constants
#param k :=1;
#initial conditions for the differential variables
param s_init:=0;
param x_init:=0;
param y_init:=0;
param p_init:=0;
param t_init:=0;


#define indices and number of collocation points
#and finite elements

param Ne >= 1 integer;
param Nc >= 1 integer;
let Ne:=3;
let Nc:=3;
set e:= 0..Ne; #elemets
set cp:=1..Nc; # collocation points


#coefficients of Radau collocation 
param a {cp,cp};
let a[1,1] := 0.19681547722366;
let a[1,2] := 0.39442431473909;
let a[1,3] :=  0.37640306270047;
let a[2,1] := -0.06553542585020;
let a[2,2] := 0.29207341166523;
let a[2,3] := 0.51248582618842;
let a[3,1]:= 0.02377097434822;
let a[3,2]:=-0.04154875212600;
let a[3,3] := 0.11111111111111;

#define discretized profile for state variables

var s{e,cp} >= 0 := s_init;

var x{e,cp} >= 0 := x_init;

var y{e,cp} >= 0 := y_init;

var p{e,cp} >= 0 := p_init;

var t{e,cp} >=0 :=t_init;

param h= 1/Ne;

var t_f = t[Ne,Nc];


#define discretized derivatives for state variables

var sdot{e,cp};
var xdot{e,cp};
var ydot{e,cp};
var pdot{e,cp};
var tdot{e,cp};

# define discretized profiles for control variables

var u_1{e} >= 0, <= 1, :=0;

var u_2{e} >= 0, <= 1, :=0;

var u_3{e} >= 0, <= 1, :=0;

#algebraic equations given as direct assignments


#differential equation model

odeS{i in e, j in cp}:
sdot[i,j]=-u_1[i]*s[i,j];

odeX{i in e, j in cp}:
xdot[i,j]=(u_1[i]*s[i,j])-(x[i,j]*u_2[i]);

odeY{i in e, j in cp}:
ydot[i,j]=(u_2[i]*x[i,j])-(y[i,j]*u_3[i]);

odeP{i in e, j in cp}:
pdot[i,j]=y[i,j]*u_3[i];

odeT{i in e, j in cp}:
tdot[i,j]=1;

#continuity of differential states accross finite elements
#variable definition at collocation points for elements
#from second element to last element
#i=2,Ne

ecolS{i in e diff{0,1}, j in cp}: 
s[i,j]=s[i-1,Nc]+t_f*h*sum{k in cp}( a[k,j]*sdot[i,k]);

ecolX{i in e diff{0,1}, j in cp}: 
x[i,j]=x[i-1,Nc]+t_f*h*sum{k in cp} (a[k,j]*xdot[i,k]);

ecolY{i in e diff{0,1}, j in cp}: 
y[i,j]=y[i-1,Nc]+t_f*h*sum{k in cp} (a[k,j]*ydot[i,k]);

ecolP{i in e diff{0,1}, j in cp}: 
p[i,j]=p[i-1,Nc]+t_f*h*sum{k in cp} (a[k,j]*pdot[i,k]);

ecolT{i in e diff{0,1},j in cp}:
t[i,j]=t[(i-1),Nc]+t_f*h*(sum{k in cp} (a[k,j]*tdot[i,k]));

#continuity of differential states accross finite elements
#variable definition at collocation points
#for first element

ecolSO{i in 0..0, j in cp}: 
s[i,j]=1;

ecolXO{i in 0..0, j in cp}: 
x[i,j]=0;

ecolYO{i in 0..0, j in cp}: 
y[i,j]=0;

ecolPO{i in 0..0, j in cp}: 
p[i,j]=0;

ecolTO{i in 0..0, j in cp}: 
t[i,j]=0;

ecolSI{i in 1..1, j in cp}: 
s[i,j]=s[0,3]+t_f*h*sum{k in cp}(a[k,j]*sdot[i,k]);

ecolXI{i in 1..1, j in cp}: 
x[i,j]=x[0,3]+t_f*h*sum{k in cp}(a[k,j]*xdot[i,k]);

ecolYI{i in 1..1, j in cp}: 
y[i,j]=y[0,3]+t_f*h*sum{k in cp}a[k,j]*ydot[i,k];

ecolPI{i in 1..1, j in cp}: 
p[i,j]=p[0,3]+t_f*h*sum{k in cp}a[k,j]*pdot[i,k];

ecolTI{i in 1..1, j in cp}: 
t[i,j]=t[0,3]+t_f*h*sum{k in cp}a[k,j]*tdot[i,k];



ecolUO{i in 0..0, j in 3..3}: 
u_1[i]=0;
ecolVO{i in 0..0, j in 3..3}: 
u_2[i]=0;
ecolZO{i in 0..0, j in 3..3}: 
u_3[i]=0;

#objective
# kann noch mit summernzeichen gekürzt werden 
#approximation wie in thesis beschrieben (trapez figure)
minimize tau : 
 (t[1,1]-t[0,3])*((s[0,3]+s[1,1])/2)+ 
 (t[1,2]-t[1,1])*((s[1,2]+s[1,1])/2)+ 
  (t[1,3]-t[1,2])*((s[1,3]+s[1,2])/2)+ 
   (t[2,1]-t[1,3])*((s[2,1]+s[1,3])/2)+ 
    (t[2,2]-t[2,1])*((s[2,2]+s[2,1])/2)+ 
    (t[2,3]-t[2,2])*((s[2,3]+s[2,2])/2)+ 
    (t[3,1]-t[2,3])*((s[3,1]+s[2,3])/2)+ 
    (t[3,2]-t[3,1])*((s[3,2]+s[3,1])/2)+ 
    (t[3,3]-t[3,2])*((s[3,3]+s[3,2])/2)+ 
     (t[1,1]-t[0,3])*((x[0,3]+x[1,1])/2)+ 
 (t[1,2]-t[1,1])*((x[1,2]+x[1,1])/2)+ 
  (t[1,3]-t[1,2])*((x[1,3]+x[1,2])/2)+ 
   (t[2,1]-t[1,3])*((x[2,1]+x[1,3])/2)+ 
    (t[2,2]-t[2,1])*((x[2,2]+x[2,1])/2)+ 
    (t[2,3]-t[2,2])*((x[2,3]+x[2,2])/2)+ 
    (t[3,1]-t[2,3])*((x[3,1]+x[2,3])/2)+ 
    (t[3,2]-t[3,1])*((x[3,2]+x[3,1])/2)+ 
    (t[3,3]-t[3,2])*((x[3,3]+x[3,2])/2)+ 
     (t[1,1]-t[0,3])*((y[0,3]+y[1,1])/2)+ 
 (t[1,2]-t[1,1])*((y[1,2]+y[1,1])/2)+ 
  (t[1,3]-t[1,2])*((y[1,3]+y[1,2])/2)+ 
   (t[2,1]-t[1,3])*((y[2,1]+y[1,3])/2)+ 
    (t[2,2]-t[2,1])*((y[2,2]+y[2,1])/2)+ 
    (t[2,3]-t[2,2])*((y[2,3]+y[2,2])/2)+ 
    (t[3,1]-t[2,3])*((y[3,1]+y[2,3])/2)+ 
    (t[3,2]-t[3,1])*((y[3,2]+y[3,1])/2)+ 
    (t[3,3]-t[3,2])*((y[3,3]+y[3,2])/2);


subject to Enzyme_tot {i in e}:				    u_1[i]+u_2[i]+u_3[i]==1;
subject to Metabolite_tot {i in e, j in cp}:	s[i,j]+x[i,j]+y[i,j]+p[i,j]<=1;
subject to Endpoint_constr1 :					p[Ne,Nc]>=0.9;
subject to Endpoint_constr2 :					s[Ne,Nc]<=0.05;


option solver ipopt;

solve;

printf {i in e, j in cp}
  '%f,%f,%f,%f,%f,\n',
  t[i, j],s[i,j],x[i,j],y[i,j],p[i,j]>> /home/fifidick/Documents/Uni_MSc/Thesis/NLP/ampl_model_outputs/v2/model_met_taumod.out;
printf {i in e}
  '%f,%f,%f,%f,\n',
  t[i,3],u_1[i],u_2[i],u_3[i]>> /home/fifidick/Documents/Uni_MSc/Thesis/NLP/ampl_model_outputs/v2/model_enz_taumod.out;
printf '%f,\n',
tau >>  /home/fifidick/Documents/Uni_MSc/Thesis/NLP/ampl_model_outputs/v2/tau.out;
#for {i in e, j in cp} {print t[i,j],',',s[i,j],',',x[i,j],',',y[i,j],',',p[i,j],','>>'/home/fifidick/Documents/Uni_MSc/Thesis/NLP/model_outputs/model.out'};
close model_met_taumod.out;

close model_enz_taumod.out;
close tau.out;
