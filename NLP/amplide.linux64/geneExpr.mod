#int CgeneExpr::model() {
	param i;
	param j;
	VAR_VECTOR x("x",REAL_GE,n + 1,m + 1), e("e",REAL_GE,n,m),
			u("u",REAL_GE,n,m), v("v",REAL_GE,n,m),
			y("y",REAL_GE,n,m), z("z",REAL_GE,n,m);
	x0 = &x; e0 = &e;

	maximize(x(n,m));

	forall(j in [0,m))
		x(0,j+1)-x(0,j) == -(0.5*k[0])*(z(0,j)-u(0,j)-v(0,j));

	forall(i in [1,n), j in [0,m))
		x(i,j+1)-x(i,j) ==   (0.5*k[i-1])*(z(i-1,j)-u(i-1,j)-v(i-1,j))
		                   - (0.5*k[i])*(z(i,j)-u(i,j)-v(i,j));

	forall(j in [0,m))
		x(n,j+1)-x(n,j) == (0.5*k[n-1])*(z(n-1,j)-u(n-1,j)-v(n-1,j));

	forall(j in [0,m))
		sum(i in [0,n)) e(i,j) == Etot;


	forall(i in [0,n), j in [0,m)) {
		y(i,j) == x(i,j) + e(i,j);
		z(i,j) == function(y(i,j),"(0.0,0.0),(0.25,0.125),(0.5,0.25),(0.75,0.5625),(1.0,1.0),(1.25,1.5625),(1.5,2.25),(1.75,3.0625),(2.0,4.0)");
		u(i,j) == function(e(i,j),"(0.0,0.0),(0.25,0.125),(0.5,0.25),(0.75,0.5625),(1.0,1.0)");
		v(i,j) == function(x(i,j),"(0.0,0.0),(0.25,0.125),(0.5,0.25),(0.75,0.5625),(1.0,1.0)");
	}

	x(0,0) == C;

	forall(i in [1,n])
		x(i,0) == 0;

	forall(i in [0,n], j in [1,m])
		x(i,j) <= C;
		
	forall(i in [0,n), j in [0,m)) {
		e(i,j) <= Etot;
		y(i,j) <= C+Etot;
		z(i,j) <= (C+Etot)*(C+Etot);
		u(i,j) <= Etot;
		v(i,j) <= C;
	}

	preprocoff();
	setRoundingType(ROUND_USER);
	optimize("geneExpr.sol");
//	printsol("geneExpr.sol");
	#return 0;
#} // end of CgeneExpr::model()

