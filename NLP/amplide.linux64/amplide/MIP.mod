param k =1;
param Etot = 1;

param n >= 1 integer;
param m >= 1 integer;

let n := 5; #reactions -->interval number
let m:=180;# time points

# Indice Variantions
set e:= 0..n-1;#elements
set p:= 0..m-1; #points

set o := 0..m;
set e_x := 0..n;

set p_x := 1..m;
set l := 1..n;

set q := 1..n-1; 

#variable declaration

var X{0..n+1,0..m+1} >= 0 ;
var E{e_x,o} >= 0 ;
var U{e_x,o} >= 0 ;
var V{e_x,o} >= 0 ;
var Y{e_x,o}>= 0 ;
var Z{e_x,o} >= 0 ;

#objective
maximize tau : X[n,m];

#
subject to X0_c {j in p_x}:
	(X[0,j+1]-X[0,j] == -(0.5*k)*(Z[0,j]-U[0,j]-V[0,j]));


subject to X_c {i in q , j in p }:
	X[i,j+1]-X[i,j] == (0.5*k)*(Z[i-1,j]-U[i-1,j]-V[i-1,j])-(0.5*k)*(Z[i,j]-U[i,j]-V[i,j]);


subject to XN_c {j in p_x}:
	X[n,j+1]-X[n,j] == (0.5*k)*(Z[n-1,j]-U[n-1,j]-V[n-1,j]);
	
subject to Y_c { i in e, j in p}:
Y[i,j] == X[i,j]+E[i,j];


# 	<<breakpoints;slopes>>
subject to Z_c { i in e , j in p }:
Z[i,j] = << 0.25, 0.5, 0.75 , 1, 1.25, 1.5,1.75; 0.25, 0.75, 1.25, 1.75, 2.25, 2.75, 3.25, 3.75  >> Y[i,j];
subject to V_c { i in e , j in p }:
V[i,j] = << 0.25, 0.5, 0.75 ; 0.25, 0.75, 1.25, 1.75  >> X[i,j];
subject to U_c{ i in e , j in p }:
U[i,j] = << 0.25, 0.5, 0.75 ; 0.25, 0.75, 1.25, 1.75 >> E[i,j];

#constraints
subject to EnzymeAmount {j in p_x}:
	sum{i in e diff{n}} E[i,j] == Etot;
	
subject to init: X[0,0]== 1;

subject to initX {i in l}: X[i,0] == 0;

subject to initX_l {i in e_x, j in p_x}: X[i,j] <= 1;

subject to E_max {i in e diff{n}, j in p diff{m}} : 
E[i,j] <= 1;

subject to Y_max_ {i in e , j in p} : 
Y[i,j] <= 1+ Etot;
subject to Z_max {i in e , j in p } : 
Z[i,j] <= (1+Etot) * (1+ Etot);
subject to U_max {i in e , j in p } : 
U[i,j] <= Etot;
subject to  V_max{i in e , j in p } : 
V[i,j] <= 1;


option solver cplex;

solve;