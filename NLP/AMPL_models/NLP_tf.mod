#define constants
#param k :=1;
#initial conditions for the differential variables
param s_init:=1;
param x_init:=0;
param y_init:=0;
param p_init:=0;
param t_init:=0;


#define indices and number of collocation points
#and finite elements

param Ne >= 1 integer;
param Nc >= 1 integer;
let Ne:=3;
let Nc:=3;
set e:= 1..Ne; #elemets
set cp:=1..Nc; # collocation points


#coefficients of Radau collocation 
param a {cp,cp};
let a[1,1] := 0.19681547722366;
let a[1,2] := 0.39442431473909;
let a[1,3] :=  0.37640306270047;
let a[2,1] := -0.06553542585020;
let a[2,2] := 0.29207341166523;
let a[2,3] := 0.51248582618842;
let a[3,1]:= 0.02377097434822;
let a[3,2]:=-0.04154875212600;
let a[3,3] := 0.11111111111111;

#define discretized profile for state variables

var s{e,cp} >= 0 := s_init;

var x{e,cp} >= 0 := x_init;

var y{e,cp} >= 0 := y_init;

var p{e,cp} >= 0 := p_init;

var t{e,cp} >=0 :=t_init;

param h= 1/Ne;

var t_f = t[Ne,Nc];


#define discretized derivatives for state variables

var sdot{e,cp};
var xdot{e,cp};
var ydot{e,cp};
var pdot{e,cp};
var tdot{e,cp};

# define discretized profiles for control variables

var u_1{e} >= 0, <= 1, :=1;

var u_2{e} >= 0, <= 1, :=0;

var u_3{e} >= 0, <= 1, :=0;

#algebraic equations given as direct assignments


#differential equation model

odeS{i in e, j in cp}:
sdot[i,j]=-u_1[i]*s[i,j];

odeX{i in e, j in cp}:
xdot[i,j]=u_1[i]*s[i,j]-x[i,j]*u_2[i];

odeY{i in e, j in cp}:
ydot[i,j]=x[i,j]*u_2[i]-y[i,j]*u_3[i];

odeP{i in e, j in cp}:
pdot[i,j]=y[i,j]*u_3[i];

odeT{i in e, j in cp}:
tdot[i,j]=1;

#continuity of differential states accross finite elements
#variable definition at collocation points for elements
#from second element to last element
#i=2,Ne

ecolS{i in e diff{1}, j in cp}: 
s[i,j]=s[i-1,Nc]+t_f*h*sum{k in cp} a[k,j]*sdot[i,k];

ecolX{i in e diff{1}, j in cp}: 
x[i,j]=x[i-1,Nc]+t_f*h*sum{k in cp} a[k,j]*xdot[i,k];

ecolY{i in e diff{1}, j in cp}: 
y[i,j]=y[i-1,Nc]+t_f*h*sum{k in cp} a[k,j]*ydot[i,k];

ecolP{i in e diff{1}, j in cp}: 
p[i,j]=p[i-1,Nc]+t_f*h*sum{k in cp} a[k,j]*pdot[i,k];

ecolT{i in e diff{1},j in cp}:
t[i,j]=t[(i-1),Nc]+t_f*h*(sum{k in cp} a[k,j]*tdot[i,k]);

#continuity of differential states accross finite elements
#variable definition at collocation points
#for first element

ecolSO{i in 1..1, j in cp}: 
s[i,j]=s_init+t_f*h*sum{k in cp}a[k,j]*sdot[i,k];

ecolXO{i in 1..1, j in cp}: 
x[i,j]=x_init+t_f*h*sum{k in cp}a[k,j]*xdot[i,k];

ecolYO{i in 1..1, j in cp}: 
y[i,j]=y_init+t_f*h*sum{k in cp}a[k,j]*ydot[i,k];

ecolPO{i in 1..1, j in cp}: 
p[i,j]=p_init+t_f*h*sum{k in cp}a[k,j]*pdot[i,k];

ecolTO{i in 1..1, j in cp}: 
t[i,j]=t_init+t_f*h*sum{k in cp}a[k,j]*tdot[i,k];


#objective

minimize tau : 
 (t[1,1]-t_init)*((s_init+s[1,1])/2)+ 
sum{i in e, j in cp diff{1}}((t[i,j]-t[i,j-1])*(s[i,j-1]+s[i,j])/2)+
 (t[1,1]-t_init)*((x_init+x[1,1])/2)+ 
sum{i in e, j in cp diff{1}}((t[i,j]-t[i,j-1])*(x[i,j-1]+x[i,j])/2)+
 (t[1,1]-t_init)*((y_init+y[1,1])/2)+ 
sum{i in e, j in cp diff{1}}((t[i,j]-t[i,j-1])*(y[i,j-1]+y[i,j])/2);



subject to Enzyme_tot {i in e}:				    u_1[i]+u_2[i]+u_3[i]=1;
#subject to Metabolite_tot {i in e, j in cp}:	s[i,j]+x[i,j]+y[i,j]+p[i,j]<=1;
#subject to Endpoint_constr1 :					s[Ne,Nc]<=0.1;
subject to Endpoint_constr2 :					p[Ne,Nc]=0.9;


option solver ipopt;

solve;









