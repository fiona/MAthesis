function dx = dyneqn1( t,x,u,theta,beta,ks )
%DYNEQN1 dynmaic equation of the system with piecewise constant control
dx = zeros(5,1);
dx(1) = x(3);
dx(2) = x(4);
dx(3) = cos(theta(ks))*u(ks) + (beta*(3-x(1)))/(x(2)^2+(x(1)-3)^2)^(3/2);
dx(4) = sin(theta(ks))*u(ks) + (-beta*x(2))/(x(2)^2+(x(1)-3)^2)^(3/2);
dx(5) = 10*u(ks)^2+4*theta(ks)^2;

end