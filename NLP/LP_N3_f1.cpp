/*
 *    This file is part of ACADO Toolkit.
 *
 *    ACADO Toolkit -- A Toolkit for Automatic Control and Dynamic Optimization.
 *    Copyright (C) 2008-2009 by Boris Houska and Hans Joachim Ferreau, K.U.Leuven.
 *    Developed within the Optimization in Engineering Center (OPTEC) under
 *    supervision of Moritz Diehl. All rights reserved.
 *
 *    ACADO Toolkit is free software; you can redistribute it and/or
 *    modify it under the terms of the GNU Lesser General Public
 *    License as published by the Free Software Foundation; either
 *    version 3 of the License, or (at your option) any later version.
 *
 *    ACADO Toolkit is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *    Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU Lesser General Public
 *    License along with ACADO Toolkit; if not, write to the Free Software
 *    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */



 /**
 *    \file   examples/getting_started/simple_ocp.cpp
 *    \author Boris Houska, Hans Joachim Ferreau
 *    \date   2009
 */


#include <acado_optimal_control.hpp>
#include <gnuplot/acado2gnuplot.hpp>

double clock1 = clock();

int main( ){

    USING_NAMESPACE_ACADO


    DifferentialState        y1, y2, y3, y4,y5;  // the differential states
    Control                  u1; // the control input u
    Control                  u2;   
    Control                  u3;
    Parameter                T1,T2,T3;
    DifferentialEquation     f(0.0, T1);     // the differential equation
    DifferentialEquation     g(T1, T2);
    DifferentialEquation     h(T2, T3);


//  -------------------------------------
    f << dot(y1) == -y1*u1;                         // an implementation
    f << dot(y2) == y1*u1-y2*u2;             // of the model equations
    f << dot(y3) == y2*u2-y3*u3;
    f << dot(y4) == y3*u3;
    f << dot(y5) == (1-y4);

    g << dot(y1) == -y1*u1;                         // an implementation
    g << dot(y2) == y1*u1-y2*u2;             // of the model equations
    g << dot(y3) == y2*u2-y3*u3;
    g << dot(y4) == y3*u3;
    g << dot(y5) == (1-y4);

    h << dot(y1) == -y1*u1;                         // an implementation
    h << dot(y2) == y1*u1-y2*u2;             // of the model equations
    h << dot(y3) == y2*u2-y3*u3;
    h << dot(y4) == y3*u3;
    h << dot(y5) == (1-y4); 


    OCP ocp;
    ocp.minimizeMayerTerm((y5));
    ocp.subjectTo( f, 1);
    ocp.subjectTo( g, 1);
    ocp.subjectTo( h, 1);

                // Constrains at start or end
    ocp.subjectTo( AT_START, y1 ==  1.0 );     
    ocp.subjectTo( AT_START, y2 ==  0.0 );     
    ocp.subjectTo( AT_START, y3 ==  0.0 );
    ocp.subjectTo( AT_START, y4 ==  0.0 );
    ocp.subjectTo( AT_START, y5 ==  0.0 );
    ocp.subjectTo( AT_END, y4 ==  0.9 );

    ocp.subjectTo( 0.0 <= u1 <=  1.0   ); // Path Constrains
    ocp.subjectTo( 0.0 <= u2 <=  1.0   );
    ocp.subjectTo( 0.0 <= u3 <=  1.0   );
    ocp.subjectTo( y1 >=  0.0   );
    ocp.subjectTo( y2 >=  0.0   );
    ocp.subjectTo( y3 >=  0.0  );


    ocp.subjectTo( 0.0 <= T1 <=  11.0   );
    ocp.subjectTo( 0.0 <= T2 <=  21.0   );
    ocp.subjectTo( 0.0 <= T3 <=  31.0   );


    ocp.subjectTo( u1+u2+u3 <=  1.0   );     // the control input u,
  
//  -------------------------------------

   // GnuplotWindow window;
     //   window.addSubplot( y1, "y1" );
       // window.addSubplot( y2, "y2" );
  //      window.addSubplot( y3, "y3" );
   //     window.addSubplot( y4, "y4" );
   //     window.addSubplot( u1, "u1" );
   //     window.addSubplot( u2, "u2" );
   //     window.addSubplot( u3, "u3" );

       
    OptimizationAlgorithm algorithm(ocp);  // the optimization algorithm

     // Options algorithm

    algorithm.set( DISCRETIZATION_TYPE, MULTIPLE_SHOOTING  );
    algorithm.set( MAX_NUM_ITERATIONS, 120 );
    algorithm.set( KKT_TOLERANCE, 1e-5);
    algorithm.set( HESSIAN_APPROXIMATION, FULL_BFGS_UPDATE );
    algorithm.set( INTEGRATOR_TYPE, INT_BDF );

    // setup a logging object for all states, controls,  
    // disturbances and parameters with a Matlab style result
    // Estas lineas son para guardar en matlab.
    LogRecord logRecord( LOG_AT_END,"LP_n3_f1_r100.m" );
    logRecord.addItem(LOG_DIFFERENTIAL_STATES,PS_MATLAB,"STATES");
    logRecord.addItem(LOG_CONTROLS,PS_MATLAB,"CONTROLS");
    logRecord.addItem(LOG_DISTURBANCES,PS_MATLAB,"DISTURBANCES");
    logRecord.addItem(LOG_PARAMETERS,PS_MATLAB,"PARAMETERS");

    algorithm << logRecord;

 //   algorithm << window;
    algorithm.solve();

    // get the logging object back and print it
    // Esto es para que me de el archivo 
    algorithm.getLogRecord( logRecord );
    logRecord.print( );

double clock2 = clock();
printf("total computation time = %.16e \n", (clock2-clock1)/CLOCKS_PER_SEC  );

    return 0;
}
