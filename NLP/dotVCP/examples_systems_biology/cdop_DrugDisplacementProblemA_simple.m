clear mex; clear all; close all;
% --------------------------------------------------- %
% Initialization:
% --------------------------------------------------- %
data.name                 = 'DrugDisplacementProblemA_simple';

% --------------------------------------------------- %
% Settings for IVP (ODEs, sensitivities):
% --------------------------------------------------- %
data.odes.res(1)          = {'((1+0.2*(y(1)+y(2)))^2/(((1+0.2*(y(1)+y(2)))^2+232+46.4*y(2))*((1+0.2*(y(1)+y(2)))^2+232+46.4*y(1))-2152.96*y(1)*y(2)))*(((1+0.2*(y(1)+y(2)))^2+232+46.4*y(1))*(0.02-y(1))+46.4*y(1)*(u(1)-2*y(2)))'};
data.odes.res(2)          = {'((1+0.2*(y(1)+y(2)))^2/(((1+0.2*(y(1)+y(2)))^2+232+46.4*y(2))*((1+0.2*(y(1)+y(2)))^2+232+46.4*y(1))-2152.96*y(1)*y(2)))*(((1+0.2*(y(1)+y(2)))^2+232+46.4*y(2))*(u(1)-2*y(2))+46.4*(0.02-y(1)))'};
data.odes.res(3)          = {'1'};
data.odes.ic              = [0.02 0.0 0.0];
data.odes.tf              = 500.0; %final time
data.odes.NonlinearSolver = 'Functional'; %['Newton'|'Functional'] /Newton for stiff problems; Functional for non-stiff problems
data.odes.RelTol          = 1*10^(-8); %IVP relative tolerance level
data.odes.AbsTol          = 1*10^(-8); %IVP absolute tolerance level
data.sens.SensAbsTol      = 1*10^(-8); %absolute tolerance for sensitivity variables

% --------------------------------------------------- %
% NLP definition:
% --------------------------------------------------- %
data.nlp.RHO              = 5; %number of time intervals
data.nlp.J0               = 'y(3)'; %cost function: min-max(cost function)
data.nlp.u0               = 4.0; %initial value for control values
data.nlp.lb               = 0.0; %lower bounds for control values
data.nlp.ub               = 8.0; %upper bounds for control values
data.nlp.solver           = 'IPOPT'; %['FMINCON'|'IPOPT'|'SRES'|'DE'|'ACOMI'|'MISQP'|'MITS']
data.nlp.FreeTime         = 'on'; %['on'|'off'] set 'on' if free time is considered

% --------------------------------------------------- %
% Equality constraints (ECs):
% --------------------------------------------------- %
data.nlp.eq.status        = 'on'; %['on'|'off'] ECs
data.nlp.eq.NEC           = 2; %number of active ECs
data.nlp.eq.eq(1)         = {'y(1)-0.02'};
data.nlp.eq.eq(2)         = {'y(2)-2.0'};
data.nlp.eq.time(1)       = data.nlp.RHO;
data.nlp.eq.time(2)       = data.nlp.RHO;

% --------------------------------------------------- %
% Options for setting of the final output:
% --------------------------------------------------- %
data.options.trajectories = 2; %how many state trajectories will be displayed

% --------------------------------------------------- %
% Call of the main function (you do not change this!):
% --------------------------------------------------- %
[data]=dotcvp_main(data);