% A MATLAB example described in detail in the technical report
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
% DOTcvp - Dynamic Optimization Toolbox with CVP approach for          %
%  handling continuous and mixed-integer dynamic optimization problems %
% Copyright (C) 2007-2010                                              %
% Tomas Hirmajer et al., thirmajer@gmail.com                           %
%                                                                      %
% The DOTcvp toolbox is completely free of charge under the creative   %
% commons license. The conditions of the license can be found on the   %
% following web page:                                                  %
% http://creativecommons.org/licenses/by-nc-nd/3.0/                    %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %

clear mex; clear all; close all;
% --------------------------------------------------- %
% Initialization:
% --------------------------------------------------- %
data.name                 = 'DrugDisplacementProblemB';

% --------------------------------------------------- %
% Settings for IVP (ODEs, sensitivities):
% --------------------------------------------------- %
data.odes.res(1)          = {'((1+0.2*(y(1)+y(2)))^2/(((1+0.2*(y(1)+y(2)))^2+232+46.4*y(2))*((1+0.2*(y(1)+y(2)))^2+232+46.4*y(1))-2152.96*y(1)*y(2)))*(((1+0.2*(y(1)+y(2)))^2+232+46.4*y(1))*(0.02-y(1))+46.4*y(1)*(u(1)-2*y(2)))'};
data.odes.res(2)          = {'((1+0.2*(y(1)+y(2)))^2/(((1+0.2*(y(1)+y(2)))^2+232+46.4*y(2))*((1+0.2*(y(1)+y(2)))^2+232+46.4*y(1))-2152.96*y(1)*y(2)))*(((1+0.2*(y(1)+y(2)))^2+232+46.4*y(2))*(u(1)-2*y(2))+46.4*(0.02-y(1)))'};
data.odes.res(3)          = {'1'};
data.odes.ic              = [0.02 0.0 0.0];
data.odes.tf              = 300.0; %final time
data.odes.LMM             = 'BDF'; %['Adams'|'BDF'] /Adams for non-stiff problems; BDF for stiff problems

% --------------------------------------------------- %
% NLP definition:
% --------------------------------------------------- %
data.nlp.RHO              = 10; %number of time intervals
data.nlp.J0               = '0.003*y(3)'; %cost function: min-max(cost function)
data.nlp.u0               = [4.0]; %initial value for control values
data.nlp.lb               = [0.0]; %lower bounds for control values
data.nlp.ub               = [8.0]; %upper bounds for control values
data.nlp.solver           = 'MISQP'; %['FMINCON'|'IPOPT'|'SRES'|'DE'|'ACOMI'|'MISQP'|'MITS']
data.nlp.NLPtol           = 1*10^(-5); %NLP tolerance level
data.nlp.FreeTime         = 'on'; %['on'|'off'] set 'on' if free time is considered
data.nlp.lbTime           = 5.0; %lower bound of the time intervals

% --------------------------------------------------- %
% Equality constraints (ECs):
% --------------------------------------------------- %
data.nlp.eq.status        = 'on'; %['on'|'off'] ECs
data.nlp.eq.NEC           = 2; %number of active ECs
data.nlp.eq.eq(1)         = {'y(1)-0.02'};
data.nlp.eq.eq(2)         = {'y(2)-2.0'};
data.nlp.eq.time(1)       = data.nlp.RHO;
data.nlp.eq.time(2)       = data.nlp.RHO;
data.nlp.eq.PenaltyFun    = 'on'; %['on'|'off'] ECs penalty function
data.nlp.eq.PenaltyCoe    = [150.0 2.0]; %J0=J0+data.nlp.eq.PenaltyCoe*ViolationOfEqualityConstraint /* only for stochastic solvers */

% --------------------------------------------------- %
% Inequality /path/ constraints (INECs):
% --------------------------------------------------- %
data.nlp.ineq.status      = 'on'; %['on'|'off'] INECs
data.nlp.ineq.NEC         = 1; %number of active INECs
data.nlp.ineq.InNUM       = 0; %how many inequality constraints are '>' else '<'
data.nlp.ineq.eq(1)       = {'y(1)-0.026'};
data.nlp.ineq.Tol         = 0.000001; %tolerance level of violation of INECs
data.nlp.ineq.PenaltyFun  = 'on'; %['on'|'off'] INECs penalty function
data.nlp.ineq.PenaltyCoe  = [10^(5)]; %J0=J0+data.nlp.ineq.PenaltyCoe*ViolationOfInequalityConstraint /* for every inequality constraint one parameter */

% --------------------------------------------------- %
% Options for setting of the final output:
% --------------------------------------------------- %
data.options.trajectories = 2; %how many state trajectories will be displayed

data.options.action       = 'hybrid-strategy'; %['single-optimization'|'re-optimization'|'hybrid-strategy'|'simulation']

% --------------------------------------------------- %
% Call of the main function (you do not change this!):
% --------------------------------------------------- %
[data]=dotcvp_main(data);