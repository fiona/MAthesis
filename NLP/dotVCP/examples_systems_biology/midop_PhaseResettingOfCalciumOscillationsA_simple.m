% A MATLAB example described in detail in the technical report
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %
% DOTcvp - Dynamic Optimization Toolbox with CVP approach for          %
%  handling continuous and mixed-integer dynamic optimization problems %
% Copyright (C) 2007-2010                                              %
% Tomas Hirmajer et al., thirmajer@gmail.com                           %
%                                                                      %
% The DOTcvp toolbox is completely free of charge under the creative   %
% commons license. The conditions of the license can be found on the   %
% following web page:                                                  %
% http://creativecommons.org/licenses/by-nc-nd/3.0/                    %
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %

clear mex; clear all; close all;
% --------------------------------------------------- %
% Initialization:
% --------------------------------------------------- %
data.name                 = 'PhaseResettingOfCalciumOscillationsA_simple';

% --------------------------------------------------- %
% Settings for IVP (ODEs, sensitivities):
% --------------------------------------------------- %
data.odes.res(1)          = {'0.09+2.30066*y(1)-(0.64*y(1)*y(2))/(y(1)+0.19)-(4.88*y(1)*y(3))/(y(1)+1.18)'};
data.odes.res(2)          = {'2.08*y(1)-(32.24*y(2))/(y(2)+29.09)'};
data.odes.res(3)          = {'(5.0*y(2)*y(3)*y(4))/(y(4)+2.67)+0.7*y(2)+13.58*y(1)-(4.85*y(3))/(y(3)+0.05)+y(4)/10-(u(1)*(153.0*y(3)/(p(1)*y(3)+0.16))+(1-u(1))*(153.0*y(3)/(y(3)+0.16)))'};
data.odes.res(4)          = {'-(5.0*y(2)*y(3)*y(4))/(y(4)+2.67)+(4.85*y(3))/(y(3)+0.05)-y(4)/10'};
data.odes.res(5)          = {'5*(y(1)-6.78677)^2+5*(y(2)-22.65836)^2+15*(y(3)-0.38431)^2+25*(y(4)-0.28977)^2+50*u(1)'};
data.odes.res(6)          = {'(y(1)-6.78677)^2+(y(2)-22.65836)^2+(y(3)-0.38431)^2+(y(4)-0.28977)^2'};
data.odes.ic              = [0.03966 1.09799 0.00142 1.65431 0.0 0.0];
data.odes.tf              = 22.0; %final time

% --------------------------------------------------- %
% NLP definition:
% --------------------------------------------------- %
data.nlp.RHO              = 6; %number of time intervals
data.nlp.J0               = 'y(5)'; %cost function: min-max(cost function)
data.nlp.u0               = [0]; %initial values for control values
data.nlp.lb               = [0]; %lower bounds for control values
data.nlp.ub               = [1]; %upper bounds for control values
data.nlp.p0               = [1.0]; %initial values for time-independent parameters
data.nlp.lbp              = [1.0]; %lower bounds for time-independent parameters
data.nlp.ubp              = [1.3]; %upper bounds for time-independent parameters
data.nlp.solver           = 'MITS'; %['FMINCON'|'IPOPT'|'SRES'|'DE'|'ACOMI'|'MISQP'|'MITS']
data.nlp.GradMethod       = 'None'; %['SensitivityEq'|'FiniteDifference'|'None']
data.nlp.MaxIter          = inf; %Maximum number of iterations
data.nlp.MaxCPUTime       = 60*60*1.00; %Maximum CPU time of the optimization (60*60*0.25) = 15 minutes
data.nlp.FreeTime         = 'on'; %['on'|'off'] set 'on' if free time is considered
data.nlp.NUMi             = 1; %number of integer variables (u) taken from the last control variables, if not equal to 0 you need to use some MINLP solver ['ACOMI'|'MISQP'|'MITS']

% --------------------------------------------------- %
% Equality constraints (ECs):
% --------------------------------------------------- %
data.nlp.eq.status        = 'on'; %['on'|'off'] ECs
data.nlp.eq.NEC           = 1; %number of active ECs
data.nlp.eq.eq(1)         = {'t(1)+t(2)+t(3)+t(4)+t(5)+t(6)-22.0'};
data.nlp.eq.time(1)       = data.nlp.RHO;
data.nlp.eq.PenaltyFun    = 'on'; %['on'|'off'] ECs penalty function
data.nlp.eq.PenaltyCoe    = [1.0]; %J0=J0+data.nlp.eq.PenaltyCoe*ViolationOfEqualityConstraint /* only for stochastic solvers */

% --------------------------------------------------- %
% Options for setting of the final output:
% --------------------------------------------------- %
data.options.intermediate = 'on'; %['on'|'off'|'silent'] display of the intermediate results
data.options.display      = 'on'; %['on'|'off'] display of the figures
data.options.trajectories = 4; %how many state trajectories will be displayed

% --------------------------------------------------- %
% Call of the main function (you do not change this!):
% --------------------------------------------------- %
[data]=dotcvp_main(data);