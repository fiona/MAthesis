#include <iostream>

using namespace std;

#include <cstdlib>
#include <acado_toolkit.hpp>
#include <acado_gnuplot.hpp>
#include "/home/fifidick/Documents/Uni_MSc/Thesis/NLP/ACADOtoolkit/acado/bindings/acado_gnuplot/gnuplot_window.hpp"

 void modelAndSolveN6(int no_interval)
{
USING_NAMESPACE_ACADO


    DifferentialState        s,w,x,y,z,p;     // the differential states
    Control                  i,j,k,l,m;    // the control input enzymes
    Parameter                T       ;     // the time horizon T
    DifferentialEquation     f( 0.0, T);     // the differential equation

//  -------------------------------------
    OCP ocp( 0.0,T,no_interval);                        // time horizon of the OCP: [0,T]
    ocp.minimizeLagrangeTerm(s+w+x+y+z);               // the time accumulation of metabolites is optimized (tau)
    f << dot(s) == -s*i;
    f << dot(w) == s*i-w*j;
    f << dot(x) == w*j-x*k;
    f << dot(y) == x*k-y*l;
    f << dot(z) ==  y*l-z*m;
    f << dot(p) ==  z*m;


    ocp.subjectTo( f                   );     // minimize T s.t. the model,
    ocp.subjectTo( AT_START, s ==  1.0);
     ocp.subjectTo( AT_START, w ==  0.0 );    // the initial values for s,
    ocp.subjectTo( AT_START, x ==  0.0 );
    ocp.subjectTo( AT_START, y ==  0.0 );
    ocp.subjectTo( AT_START, z ==  0.0 );
    ocp.subjectTo( AT_START, p ==  0.0 );
    ocp.subjectTo( AT_START, i ==  0.0 );
    ocp.subjectTo( AT_START, j ==  0.0 );
    ocp.subjectTo( AT_START, k ==  0.0 );
    ocp.subjectTo( AT_START, l ==  0.0 );
    ocp.subjectTo( AT_START, m ==  0.0 );

    ocp.subjectTo( AT_END  , s <= 0.1);     // the terminal constraints for s
    ocp.subjectTo( AT_END  , p >= 0.9);     // and p
    ocp.subjectTo( i+j+k+l+m ==  1.0   ); // path constraints
    ocp.subjectTo( 0.0 <= s <=  1.0   );
    ocp.subjectTo( 0.0 <= w <=  1.0   ); //upper and lower bounds
    ocp.subjectTo( 0.0 <= x <=  1.0   );
    ocp.subjectTo( 0.0 <= y <=  1.0   );
    ocp.subjectTo( 0.0 <= z <=  1.0   );
    ocp.subjectTo( 0.0 <= p <=  1.0   );
    ocp.subjectTo( 0.0 <= i <=  1.0   );
    ocp.subjectTo( 0.0 <= j <=  1.0   );
    ocp.subjectTo( 0.0 <= k <=  1.0   );
    ocp.subjectTo( 0.0 <= l <=  1.0   );
    ocp.subjectTo( 0.0 <= m <=  1.0   );
        // as well as the bounds on v
    // and the time horizon T.
//  -------------------------------------

    GnuplotWindow window  (PLOT_AT_END );
     //   window.addSubplot( s, "SUBSTRATE S"      );
        window.addSubplot(w, "W");
        window.addSubplot( x,"METABOLITE X"      );
        window.addSubplot( y, "METABOLITE Y"         );
        window.addSubplot( y, "METABOLITE Z"         );
       window.addSubplot( p, "PRODUCT P" );
        window.addSubplot( i, "Enzyme 1");
       window.addSubplot( j, "Enzyme 2");
       window.addSubplot( k, "Enzyme 3");
         window.addSubplot( l, "Enzyme 4");
         window.addSubplot(m, "e");

    OptimizationAlgorithm algorithm(ocp);
  //  algorithm.determineDimensions(f);
  //  algorithm.isLinearQuadratic(f);
//    algorithm.extractOCPdata(ocp);


//     algorithm.initializeDifferentialStates( "states.txt" );
//    algorithm.initializeControls          ( "controls.txt" );
//    algorithm.initializeParameters        ( "parameters.txt" );
algorithm.set(MAX_NUM_ITERATIONS , 5000) ;
 algorithm.set( KKT_TOLERANCE        , 1e-10           );


 // the optimization alorithm
    algorithm << window;


  algorithm.getDifferentialStates("/home/fifidick/Desktop/states.txt"    );
    algorithm.getObjectiveValue("tau.txt");

    algorithm.getParameters        ("parameters.txt");
    algorithm.getControls          ("controls.txt"  );



}

void modelAndSolveN5(int no_interval)
{
USING_NAMESPACE_ACADO


    DifferentialState        s,x,y,z,p;     // the differential states
    Control                  i,j,k,l;    // the control input enzymes
    Parameter                T       ;     // the time horizon T
    DifferentialEquation     f( 0.0, T);     // the differential equation

//  -------------------------------------
    OCP ocp( 0.0,T,no_interval);                        // time horizon of the OCP: [0,T]
    ocp.minimizeLagrangeTerm(s+x+y+z);               // the time accumulation of metabolites is optimized (tau)
    f << dot(s) == -s*i;
    f << dot(x) == s*i-x*j;
    f << dot(y) == x*j-y*k;
    f << dot(z) ==  y*k-z*l;
    f << dot(p) ==  z*l;


    ocp.subjectTo( f                   );     // minimize T s.t. the model,
    ocp.subjectTo( AT_START, s ==  1.0);     // the initial values for s,
    ocp.subjectTo( AT_START, x ==  0.0 );
    ocp.subjectTo( AT_START, y ==  0.0 );
    ocp.subjectTo( AT_START, z ==  0.0 );
    ocp.subjectTo( AT_START, p ==  0.0 );
    ocp.subjectTo( AT_START, i ==  0.0 );
    ocp.subjectTo( AT_START, j ==  0.0 );
    ocp.subjectTo( AT_START, k ==  0.0 );
    ocp.subjectTo( AT_START, l ==  0.0 );

    ocp.subjectTo( AT_END  , s <= 0.1);     // the terminal constraints for s
    ocp.subjectTo( AT_END  , p >= 0.9);     // and p
    ocp.subjectTo( i+j+k+l <=  1.0   ); // path constraints
    ocp.subjectTo( 0.0 <= s <=  1.0   ); //upper and lower bounds
    ocp.subjectTo( 0.0 <= x <=  1.0   );
    ocp.subjectTo( 0.0 <= y <=  1.0   );
    ocp.subjectTo( 0.0 <= z <=  1.0   );
    ocp.subjectTo( 0.0 <= p <=  1.0   );
    ocp.subjectTo( 0.0 <= i <=  1.0   );
    ocp.subjectTo( 0.0 <= j <=  1.0   );
    ocp.subjectTo( 0.0 <= k <=  1.0   );
    ocp.subjectTo( 0.0 <= l <=  1.0   );
        // as well as the bounds on v
    // and the time horizon T.
//  -------------------------------------

    GnuplotWindow window  (PLOT_AT_END );
        window.addSubplot( s, "SUBSTRATE S"      );
        window.addSubplot( x,"METABOLITE X"      );
        window.addSubplot( y, "METABOLITE Y"         );
        window.addSubplot( y, "METABOLITE Z"         );
       window.addSubplot( p, "PRODUCT P" );
        window.addSubplot( i, "Enzyme 1");
       window.addSubplot( j, "Enzyme 2");
       window.addSubplot( k, "Enzyme 3");
         window.addSubplot( l, "Enzyme 4");

    OptimizationAlgorithm algorithm(ocp);
//     algorithm.initializeDifferentialStates( "states.txt" );
//    algorithm.initializeControls          ( "controls.txt" );
//    algorithm.initializeParameters        ( "parameters.txt" );
algorithm.set(MAX_NUM_ITERATIONS , 5000) ;
 algorithm.set( KKT_TOLERANCE        , 1e-10           );
 algorithm.set(MIN_LINESEARCH_PARAMETER , 0.5);


 // the optimization alorithm
    algorithm << window;

    algorithm.solve();                        // solves the problem.

   // algorithm.printLoggingInfo();
    algorithm.getDifferentialStates("/home/fifidick/Desktop/states.txt"    );
    algorithm.getObjectiveValue("tau.txt");

    algorithm.getParameters        ("parameters.txt");
    algorithm.getControls          ("controls.txt"  );



}


void modelAndSolveN4(int intervals)
{
USING_NAMESPACE_ACADO


    DifferentialState        s,x,y,p;     // the differential states
    Control                  u,v,z;     // the control input enzymes
    Parameter                T       ;     // the time horizon T
    DifferentialEquation     f( 0.0, T);     // the differential equation

//  -------------------------------------
    OCP ocp( 0.0,T,intervals);                        // time horizon of the OCP: [0,T]
    ocp.minimizeLagrangeTerm(s+x+y);               // the time accumulation of metabolites is optimized (tau)
    f << dot(s) == -s*u;
    f << dot(x) == s*u-x*v;
    f << dot(y) == x*v-y*z;
    f << dot(p) ==  y*z;


    ocp.subjectTo( f                   );     // minimize T s.t. the model,
    ocp.subjectTo( AT_START, s ==  1.0);     // the initial values for s,
    ocp.subjectTo( AT_START, x ==  0.0 );
    ocp.subjectTo( AT_START, y ==  0.0 );
    ocp.subjectTo( AT_START, p ==  0.0 );
    ocp.subjectTo( AT_START, u ==  1.0 );
    ocp.subjectTo( AT_START, v ==  0.0 );
    ocp.subjectTo( AT_START, z ==  0.0 );

    ocp.subjectTo( AT_END  , s <= 0.05);     // the terminal constraints for s
    ocp.subjectTo( AT_END  , p == 0.9);     // and p
    ocp.subjectTo( u+v+z <=  1.0   ); // path constraints
    ocp.subjectTo( 0.0 <= s <=  1.0   ); //upper and lower bounds
    ocp.subjectTo( 0.0 <= x <=  1.0   );
    ocp.subjectTo( 0.0 <= y <=  1.0   );
    ocp.subjectTo( 0.0 <= p <=  1.0   );
    ocp.subjectTo( 0.0 <= u <=  1.0   );
    ocp.subjectTo( 0.0 <= v <=  1.0   );
    ocp.subjectTo( 0.0 <= z <=  1.0   );
        // as well as the bounds on v
    // and the time horizon T.
//  -------------------------------------

    GnuplotWindow window  (PLOT_AT_END );
        window.addSubplot( s, "SUBSTRATE S"      );
        window.addSubplot( x,"METABOLITE X"      );
        window.addSubplot( y, "METABOLITE Y"         );
       window.addSubplot( p, "PRODUCT P" );
        window.addSubplot( u, "Enzyme 1");
       window.addSubplot( v, "Enzyme 2");
       window.addSubplot( z, "Enzyme 3");

    OptimizationAlgorithm algorithm(ocp);
     algorithm.initializeDifferentialStates( "/home/fifidick/Desktop/states.txt" );
   // algorithm.initializeControls          ( "controls.txt" );
    //algorithm.initializeParameters        ( "parameters.txt" );
algorithm.set(MAX_NUM_ITERATIONS , 5000) ;
 algorithm.set( KKT_TOLERANCE        , 1e-10           );


 // the optimization alorithm
    algorithm << window;

    algorithm.solve();                        // solves the problem.

    //algorithm.getDifferentialStates("/home/fifidick/Desktop/states.txt"    );
    algorithm.getObjectiveValue("/home/fifidick/Desktop/tau.txt");

    algorithm.getParameters        ("/home/fifidick/Desktop/parameters.txt");
    algorithm.getControls          ("controls.txt"  );


}


int main(int argc, char *argv[])
{


    int i= 3; //number of intervals for multiple shooting
    //acadoModel3modelAndSolveN5(i);
   modelAndSolveN4(i);
exit(0);

}
