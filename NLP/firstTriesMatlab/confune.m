function [c, ceq] = confuneq(x)
% Nonlinear inequality constraints
c = -x(1)*x(2) - 10;
% Nonlinear equality constraints
ceq = x(1)^2 + x(2) - 1;
% NOTE:
%    If you have more than one inequality/equality then write them as
%      c = [... ; ... ; ...] & ceq = [... ; ... ; ...]
%    If you do not have any inequality/equality then givee them as empty sets
%      c = [] & ceq = []
% NOTE:
%    The direction and form of inequalities must be "g(x) < 0"

