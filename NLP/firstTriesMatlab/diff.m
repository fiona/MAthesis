    Nc=4;
    Ne=3;
    m=4;
    
    %Ne=number of elements
    %Nc = number of collocation points within each element
    %m = numer of metabolites
    %vector of metabolite differential equations
  
  %variables 
  
  S=sym('s',[3 4]);
  X=sym('x', [3 4]);
  Y=sym('y', [3 4]);
  P=sym('p', [3 4]);
  
  e1 =sym ('e1', [1 3]);
  e2 =sym ('e2', [1 3]);
  e3 =sym ('e3', [1 3]);
 
    
 %algebraic constraints
 g=sym({});

 for i=1:Ne
     for j = 1:Nc
         g=[g;S(i,j)+X(i,j)+Y(i,j)+P(i,j)-1];
     end
 end
 
  for i=1:Ne
     g=[g;e1(i)+e2(i)+e3(i)-1];
 end
     
    

%differential constraints
%vector of metabolite differential equations
    xdiff= sym(zeros(Ne,Nc,m));
   
 
        for i=1:Ne
            for j=1:Nc
                xdiff(i,j,1)= -S(i,j)*e1(i);
                xdiff(i,j,2)= S(i,j)*e1(i)-X(i,j)*e2(i);
                xdiff(i,j,3)= X(i,j)*e2(i)-Y(i,j)*e3(i);
                xdiff(i,j,4)= Y(i,j)*e3(i);
            end
        end
        
        h= sym('h',[1 3]);
       
       % S(1,1)=1+h(1)(LAGRANGE*xdiff(1,1,1));
        

