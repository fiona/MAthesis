
%Step 3: Invoke constrained optimization routine
x0 = [1,0,0]; % Make a starting guess at the solution
options = optimset('LargeScale','off');
[x,fval] = fmincon(@objfun,x0,[],[],[],[],[],[],@confuneq,options)
[c,ceq] = confuneq(x) % Check the constraint values at x
% NOTE:
%    If you have bounds (side constraints) on variables then you go as
%    follows:
%         lb = [0,0];   % Set lower bounds
%         ub = [ ];     % If no upper bounds
%         fmincon(@objfun,x0,[],[],[],[],lb,ub,@confun,options)
