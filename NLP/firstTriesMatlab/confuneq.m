%Step 2: Write an M-file confuneq.m for the nonlinear constraints



c=[e1+e2+e3-1;S+X+Y+P-1];
ceq=[];

% NOTE:
%    If you have more than one inequality/equality then write them as
%      c = [... ; ... ; ...] & ceq = [... ; ... ; ...]
%    If you do not have any inequality/equality then givee them as empty sets
%      c = [] & ceq = []
% NOTE:
%    The direction and form of inequalities must be "g(x) < 0"
